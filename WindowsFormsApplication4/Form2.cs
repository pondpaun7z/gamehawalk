﻿using System;
using System.Media;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication4
{
    public partial class Form2 : Form
    {
        int time = 2;
        int ch = 1;
        int vskill = 0;
        Random rand = new Random();
        private int turn;
        protected int att;
        protected byte skill_status;
        PictureBox[,] pic = new PictureBox[2, 8];

        SoundPlayer sound = new SoundPlayer(Properties.Resources.fight_sound);
        SoundPlayer sound_back = new SoundPlayer(Properties.Resources.stratgame_sound);

        public Form2(int turn, byte skill_status)
        {
            InitializeComponent();
            sound.Play();
            StartPosition = FormStartPosition.CenterScreen;

            this.turn = turn;
            this.skill_status = skill_status;
            att = rand.Next(11) + 1;

            //0 set  , 1 att , 2 def , 3 picskill , 4 none  
            pic[0, 0] = ch1set;
            pic[0, 1] = ch1att;
            pic[0, 2] = ch1def;
            pic[0, 3] = ch1picskill;
            pic[0, 4] = none;
            pic[0, 5] = skill1;

            pic[1, 0] = ch2set;
            pic[1, 1] = ch2att;
            pic[1, 2] = ch2def;
            pic[1, 3] = ch2picskill;
            pic[1, 4] = none;
            pic[1, 5] = ch2attskill;   //att normal
            pic[1, 6] = ch2ball;
            pic[1, 7] = ch2ballatt;


            // Start
            if (turn == 0)
            {
                skill_b.Image = pic[turn, 5].Image;
                pl1.Image = pic[turn, 0].Image;
                pl2.Image = pic[1, 0].Image;
            } else if (turn == 1)
            {
                skill_b.Image = pic[turn, 3].Image;
                pl1.Image = pic[turn, 0].Image;
                pl2.Image = pic[0, 0].Image;
            }
        }

        public int getAtt()
        {
            return att;
        }

        public byte getStatus()
        {
            return skill_status;
        }

        private void pl2_Click(object sender, EventArgs e)
        {
            pl2.Enabled = false;
            if (turn == 0)
            {
                skill_b.Image = pic[0, 5].Image;
                if (vskill == 0) // attact
                {
                    att = rand.Next(5, 11);
                    pl1.Location = new Point(534, 262);
                    pl1.Image = pic[0, 1].Image;
                    pl2.Image = pic[1, 2].Image;
                    timer1.Start();
                    att_show.Text = "Damage " + att + "!";
                    skill_status = 0;
                    skill_b.Enabled = true;                    
                }
                else if (vskill == 1) // skill
                {
                    att = rand.Next(15, 26);
                    pictureBox1.Visible = true;
                    pictureBox1.Image = pic[0, 3].Image;
                    pl2.Image = pic[1, 2].Image;
                    att_show.Text = "Damage: " + att + "!";
                    skill_status = 1;
                    timer1.Start();
                }
            }
            else if (turn == 1)
            {
                skill_b.Image = pic[1, 3].Image;
                if (vskill == 0)
                {
                    att = rand.Next(5, 11);
                    pictureBox1.Visible = true;
                    pictureBox1.Image = pic[1, 5].Image;
                    skill_status = 0;
                    pl2.Image = pic[0, 2].Image;
                    att_show.Text = "Damage: " + att + "!";
                    timer1.Start();
                }
                else if (vskill == 1)
                {
                    att = rand.Next(15, 26);
                    pictureBox1.Visible = true;
                    pictureBox2.Visible = true;
                    pictureBox1.Image = pic[1, 7].Image;            //ball att
                    pictureBox2.Image = pic[1, 6].Image;               //ball roll
                    pl2.Image = pic[0, 2].Image;
                    att_show.Text = "Damage: " + att + "!";
                    skill_status = 1;
                    timer1.Start();
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            time--;
            if (turn == 0)
            {
                    if (time <= 0 && vskill == 0)
                    {
                        pl1.Image = pic[0, 0].Image;
                        pl1.Location = new Point(152, 262);
                        pl2.Image = pic[1, 1].Image;
                        timer1.Stop();
                        sound_back.Play();
                        this.Close();
                    }
                    else if (time <= 0 && vskill == 1)
                    {
                        pl2.Image = pic[1, 3].Image;
                        vskill = 0;
                        pictureBox1.Visible = false;
                        skill_b.Enabled = false;
                        pl2.Image = pic[1, 1].Image;
                        sound_back.Play();
                        this.Close();
                }
            }
            else if (turn == 1)
            {
                if (time <= 0 && vskill == 0)
                {
                    pictureBox1.Visible = false;
                    pl2.Image = pic[0, 1].Image;
                    timer1.Stop();
                    sound_back.Play();
                    this.Close();
                }
                else if (time <= 0 && vskill == 1)
                {
                    pictureBox1.Visible = false;
                    pictureBox2.Visible = false;
                    skill_b.Enabled = false;
                    pl2.Image = pic[0, 1].Image;
                    sound_back.Play();
                    timer1.Stop();
                    this.Close();
                }
            }

        }

        private void skill_b_Click(object sender, EventArgs e)
        {
            if (skill_status == 0)
            {
                vskill = 1;
            } else if (skill_status == 1)
            {
                MyMessageBox.ShowBox("Skill is cooldown");
            }
                
        }
    }
}


