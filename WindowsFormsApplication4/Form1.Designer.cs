﻿namespace WindowsFormsApplication4
{
    partial class start
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.showRound = new System.Windows.Forms.Label();
            this.dices = new System.Windows.Forms.PictureBox();
            this.dices_clicked = new System.Windows.Forms.PictureBox();
            this.player1 = new System.Windows.Forms.PictureBox();
            this.player2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.hp_player1 = new System.Windows.Forms.ProgressBar();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.hp_player2 = new System.Windows.Forms.ProgressBar();
            this.name_turn = new System.Windows.Forms.Label();
            this.name_show1 = new System.Windows.Forms.Label();
            this.name_show2 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.hp_show1 = new System.Windows.Forms.Label();
            this.hp_show2 = new System.Windows.Forms.Label();
            this.show_card = new System.Windows.Forms.PictureBox();
            this.hp1 = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.gold_player1 = new System.Windows.Forms.Label();
            this.gold_player2 = new System.Windows.Forms.Label();
            this.hp2 = new System.Windows.Forms.PictureBox();
            this.gold_card = new System.Windows.Forms.PictureBox();
            this.card1 = new System.Windows.Forms.PictureBox();
            this.card2 = new System.Windows.Forms.PictureBox();
            this.card3 = new System.Windows.Forms.PictureBox();
            this.card4 = new System.Windows.Forms.PictureBox();
            this.card5 = new System.Windows.Forms.PictureBox();
            this.card7 = new System.Windows.Forms.PictureBox();
            this.card9 = new System.Windows.Forms.PictureBox();
            this.card10 = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.exit = new System.Windows.Forms.Label();
            this.arrowPlayer1 = new System.Windows.Forms.PictureBox();
            this.arrowPlayer2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dices)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dices_clicked)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.player1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.player2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.show_card)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hp1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hp2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gold_card)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.card1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.card2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.card3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.card4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.card5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.card7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.card9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.card10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arrowPlayer1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arrowPlayer2)).BeginInit();
            this.SuspendLayout();
            // 
            // showRound
            // 
            this.showRound.AutoSize = true;
            this.showRound.BackColor = System.Drawing.Color.Transparent;
            this.showRound.Font = new System.Drawing.Font("Comic Sans MS", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.showRound.ForeColor = System.Drawing.Color.White;
            this.showRound.Location = new System.Drawing.Point(419, 550);
            this.showRound.Name = "showRound";
            this.showRound.Size = new System.Drawing.Size(186, 29);
            this.showRound.TabIndex = 4;
            this.showRound.Text = "Click or Spacebar";
            this.showRound.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dices
            // 
            this.dices.BackColor = System.Drawing.Color.Transparent;
            this.dices.BackgroundImage = global::WindowsFormsApplication4.Properties.Resources.dice;
            this.dices.Location = new System.Drawing.Point(435, 432);
            this.dices.Name = "dices";
            this.dices.Size = new System.Drawing.Size(127, 110);
            this.dices.TabIndex = 8;
            this.dices.TabStop = false;
            this.dices.Click += new System.EventHandler(this.dices_Click);
            // 
            // dices_clicked
            // 
            this.dices_clicked.BackColor = System.Drawing.Color.Transparent;
            this.dices_clicked.BackgroundImage = global::WindowsFormsApplication4.Properties.Resources.diceclick;
            this.dices_clicked.Location = new System.Drawing.Point(435, 432);
            this.dices_clicked.Name = "dices_clicked";
            this.dices_clicked.Size = new System.Drawing.Size(127, 110);
            this.dices_clicked.TabIndex = 9;
            this.dices_clicked.TabStop = false;
            // 
            // player1
            // 
            this.player1.BackColor = System.Drawing.Color.Transparent;
            this.player1.BackgroundImage = global::WindowsFormsApplication4.Properties.Resources.ch2head;
            this.player1.Location = new System.Drawing.Point(146, 198);
            this.player1.Name = "player1";
            this.player1.Size = new System.Drawing.Size(36, 36);
            this.player1.TabIndex = 10;
            this.player1.TabStop = false;
            // 
            // player2
            // 
            this.player2.BackColor = System.Drawing.Color.Transparent;
            this.player2.BackgroundImage = global::WindowsFormsApplication4.Properties.Resources.ch1head;
            this.player2.Location = new System.Drawing.Point(938, 414);
            this.player2.Name = "player2";
            this.player2.Size = new System.Drawing.Size(36, 36);
            this.player2.TabIndex = 11;
            this.player2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = global::WindowsFormsApplication4.Properties.Resources.ch2walk;
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureBox1.Location = new System.Drawing.Point(25, 603);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 105);
            this.pictureBox1.TabIndex = 12;
            this.pictureBox1.TabStop = false;
            // 
            // hp_player1
            // 
            this.hp_player1.Cursor = System.Windows.Forms.Cursors.Default;
            this.hp_player1.ForeColor = System.Drawing.Color.Red;
            this.hp_player1.Location = new System.Drawing.Point(193, 647);
            this.hp_player1.Name = "hp_player1";
            this.hp_player1.Size = new System.Drawing.Size(168, 23);
            this.hp_player1.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.hp_player1.TabIndex = 13;
            this.hp_player1.Value = 50;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BackgroundImage = global::WindowsFormsApplication4.Properties.Resources._1111111;
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureBox2.Location = new System.Drawing.Point(559, 603);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(100, 105);
            this.pictureBox2.TabIndex = 14;
            this.pictureBox2.TabStop = false;
            // 
            // hp_player2
            // 
            this.hp_player2.ForeColor = System.Drawing.Color.Red;
            this.hp_player2.Location = new System.Drawing.Point(720, 647);
            this.hp_player2.Name = "hp_player2";
            this.hp_player2.Size = new System.Drawing.Size(168, 23);
            this.hp_player2.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.hp_player2.TabIndex = 15;
            this.hp_player2.Value = 50;
            // 
            // name_turn
            // 
            this.name_turn.AutoSize = true;
            this.name_turn.BackColor = System.Drawing.Color.Transparent;
            this.name_turn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.name_turn.ForeColor = System.Drawing.Color.White;
            this.name_turn.Location = new System.Drawing.Point(476, 405);
            this.name_turn.Name = "name_turn";
            this.name_turn.Size = new System.Drawing.Size(14, 17);
            this.name_turn.TabIndex = 17;
            this.name_turn.Text = "-";
            // 
            // name_show1
            // 
            this.name_show1.AutoSize = true;
            this.name_show1.BackColor = System.Drawing.Color.Transparent;
            this.name_show1.Font = new System.Drawing.Font("Comic Sans MS", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.name_show1.ForeColor = System.Drawing.Color.White;
            this.name_show1.Location = new System.Drawing.Point(141, 607);
            this.name_show1.Name = "name_show1";
            this.name_show1.Size = new System.Drawing.Size(61, 29);
            this.name_show1.TabIndex = 18;
            this.name_show1.Text = "name";
            // 
            // name_show2
            // 
            this.name_show2.AutoSize = true;
            this.name_show2.BackColor = System.Drawing.Color.Transparent;
            this.name_show2.Font = new System.Drawing.Font("Comic Sans MS", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.name_show2.ForeColor = System.Drawing.Color.White;
            this.name_show2.Location = new System.Drawing.Point(665, 607);
            this.name_show2.Name = "name_show2";
            this.name_show2.Size = new System.Drawing.Size(61, 29);
            this.name_show2.TabIndex = 19;
            this.name_show2.Text = "name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Comic Sans MS", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(141, 643);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 29);
            this.label2.TabIndex = 20;
            this.label2.Text = "HP:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Comic Sans MS", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(665, 641);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 29);
            this.label3.TabIndex = 21;
            this.label3.Text = "HP:";
            // 
            // hp_show1
            // 
            this.hp_show1.AutoSize = true;
            this.hp_show1.BackColor = System.Drawing.Color.Transparent;
            this.hp_show1.Font = new System.Drawing.Font("Comic Sans MS", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hp_show1.ForeColor = System.Drawing.Color.White;
            this.hp_show1.Location = new System.Drawing.Point(367, 641);
            this.hp_show1.Name = "hp_show1";
            this.hp_show1.Size = new System.Drawing.Size(48, 29);
            this.hp_show1.TabIndex = 22;
            this.hp_show1.Text = "HP:";
            // 
            // hp_show2
            // 
            this.hp_show2.AutoSize = true;
            this.hp_show2.BackColor = System.Drawing.Color.Transparent;
            this.hp_show2.Font = new System.Drawing.Font("Comic Sans MS", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hp_show2.ForeColor = System.Drawing.Color.White;
            this.hp_show2.Location = new System.Drawing.Point(894, 641);
            this.hp_show2.Name = "hp_show2";
            this.hp_show2.Size = new System.Drawing.Size(48, 29);
            this.hp_show2.TabIndex = 23;
            this.hp_show2.Text = "HP:";
            // 
            // show_card
            // 
            this.show_card.BackColor = System.Drawing.Color.Transparent;
            this.show_card.Location = new System.Drawing.Point(146, 145);
            this.show_card.Name = "show_card";
            this.show_card.Size = new System.Drawing.Size(264, 429);
            this.show_card.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.show_card.TabIndex = 24;
            this.show_card.TabStop = false;
            this.show_card.Visible = false;
            // 
            // hp1
            // 
            this.hp1.BackColor = System.Drawing.Color.Transparent;
            this.hp1.BackgroundImage = global::WindowsFormsApplication4.Properties.Resources.hp;
            this.hp1.Location = new System.Drawing.Point(420, 673);
            this.hp1.Name = "hp1";
            this.hp1.Size = new System.Drawing.Size(44, 44);
            this.hp1.TabIndex = 25;
            this.hp1.TabStop = false;
            this.hp1.Click += new System.EventHandler(this.hp1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Comic Sans MS", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(141, 679);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 29);
            this.label4.TabIndex = 26;
            this.label4.Text = "GOLD:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Comic Sans MS", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(665, 679);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 29);
            this.label5.TabIndex = 27;
            this.label5.Text = "GOLD:";
            // 
            // gold_player1
            // 
            this.gold_player1.AutoSize = true;
            this.gold_player1.BackColor = System.Drawing.Color.Transparent;
            this.gold_player1.Font = new System.Drawing.Font("Comic Sans MS", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gold_player1.ForeColor = System.Drawing.Color.White;
            this.gold_player1.Location = new System.Drawing.Point(224, 679);
            this.gold_player1.Name = "gold_player1";
            this.gold_player1.Size = new System.Drawing.Size(77, 29);
            this.gold_player1.TabIndex = 28;
            this.gold_player1.Text = "GOLD:";
            // 
            // gold_player2
            // 
            this.gold_player2.AutoSize = true;
            this.gold_player2.BackColor = System.Drawing.Color.Transparent;
            this.gold_player2.Font = new System.Drawing.Font("Comic Sans MS", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gold_player2.ForeColor = System.Drawing.Color.White;
            this.gold_player2.Location = new System.Drawing.Point(750, 679);
            this.gold_player2.Name = "gold_player2";
            this.gold_player2.Size = new System.Drawing.Size(77, 29);
            this.gold_player2.TabIndex = 29;
            this.gold_player2.Text = "GOLD:";
            // 
            // hp2
            // 
            this.hp2.BackColor = System.Drawing.Color.Transparent;
            this.hp2.BackgroundImage = global::WindowsFormsApplication4.Properties.Resources.hp;
            this.hp2.Location = new System.Drawing.Point(948, 673);
            this.hp2.Name = "hp2";
            this.hp2.Size = new System.Drawing.Size(44, 44);
            this.hp2.TabIndex = 30;
            this.hp2.TabStop = false;
            this.hp2.Click += new System.EventHandler(this.hp2_Click);
            // 
            // gold_card
            // 
            this.gold_card.BackgroundImage = global::WindowsFormsApplication4.Properties.Resources.สมบัติ;
            this.gold_card.Location = new System.Drawing.Point(296, 491);
            this.gold_card.Name = "gold_card";
            this.gold_card.Size = new System.Drawing.Size(19, 21);
            this.gold_card.TabIndex = 31;
            this.gold_card.TabStop = false;
            this.gold_card.Visible = false;
            // 
            // card1
            // 
            this.card1.BackgroundImage = global::WindowsFormsApplication4.Properties.Resources._11291787_600704926732693_133952701_n;
            this.card1.Location = new System.Drawing.Point(12, 386);
            this.card1.Name = "card1";
            this.card1.Size = new System.Drawing.Size(19, 21);
            this.card1.TabIndex = 32;
            this.card1.TabStop = false;
            this.card1.Visible = false;
            // 
            // card2
            // 
            this.card2.BackgroundImage = global::WindowsFormsApplication4.Properties.Resources.ขโมยผ้าป่า;
            this.card2.Location = new System.Drawing.Point(12, 414);
            this.card2.Name = "card2";
            this.card2.Size = new System.Drawing.Size(19, 21);
            this.card2.TabIndex = 33;
            this.card2.TabStop = false;
            this.card2.Visible = false;
            // 
            // card3
            // 
            this.card3.BackgroundImage = global::WindowsFormsApplication4.Properties.Resources.ต่อต่อย;
            this.card3.Location = new System.Drawing.Point(12, 441);
            this.card3.Name = "card3";
            this.card3.Size = new System.Drawing.Size(19, 21);
            this.card3.TabIndex = 34;
            this.card3.TabStop = false;
            this.card3.Visible = false;
            // 
            // card4
            // 
            this.card4.BackgroundImage = global::WindowsFormsApplication4.Properties.Resources.บริจาคเงิน;
            this.card4.Location = new System.Drawing.Point(12, 468);
            this.card4.Name = "card4";
            this.card4.Size = new System.Drawing.Size(19, 21);
            this.card4.TabIndex = 35;
            this.card4.TabStop = false;
            this.card4.Visible = false;
            // 
            // card5
            // 
            this.card5.BackgroundImage = global::WindowsFormsApplication4.Properties.Resources.บริจาคเลือด;
            this.card5.Location = new System.Drawing.Point(12, 495);
            this.card5.Name = "card5";
            this.card5.Size = new System.Drawing.Size(19, 21);
            this.card5.TabIndex = 36;
            this.card5.TabStop = false;
            this.card5.Visible = false;
            // 
            // card7
            // 
            this.card7.BackgroundImage = global::WindowsFormsApplication4.Properties.Resources.ยุงกัด;
            this.card7.Location = new System.Drawing.Point(37, 414);
            this.card7.Name = "card7";
            this.card7.Size = new System.Drawing.Size(19, 21);
            this.card7.TabIndex = 38;
            this.card7.TabStop = false;
            this.card7.Visible = false;
            // 
            // card9
            // 
            this.card9.BackgroundImage = global::WindowsFormsApplication4.Properties.Resources.ลาบ;
            this.card9.Location = new System.Drawing.Point(37, 468);
            this.card9.Name = "card9";
            this.card9.Size = new System.Drawing.Size(19, 21);
            this.card9.TabIndex = 40;
            this.card9.TabStop = false;
            this.card9.Visible = false;
            // 
            // card10
            // 
            this.card10.BackgroundImage = global::WindowsFormsApplication4.Properties.Resources.ลิงลักตังค์;
            this.card10.Location = new System.Drawing.Point(37, 495);
            this.card10.Name = "card10";
            this.card10.Size = new System.Drawing.Size(19, 21);
            this.card10.TabIndex = 41;
            this.card10.TabStop = false;
            this.card10.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Comic Sans MS", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(423, 405);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 19);
            this.label6.TabIndex = 42;
            this.label6.Text = "TURN";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // exit
            // 
            this.exit.AutoSize = true;
            this.exit.BackColor = System.Drawing.Color.Transparent;
            this.exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exit.ForeColor = System.Drawing.Color.Red;
            this.exit.Location = new System.Drawing.Point(973, -7);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(41, 46);
            this.exit.TabIndex = 43;
            this.exit.Text = "x";
            this.exit.Click += new System.EventHandler(this.exit_Click);
            // 
            // arrowPlayer1
            // 
            this.arrowPlayer1.BackColor = System.Drawing.Color.Transparent;
            this.arrowPlayer1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.arrowPlayer1.Image = global::WindowsFormsApplication4.Properties.Resources.turnArrow__2_;
            this.arrowPlayer1.Location = new System.Drawing.Point(144, 164);
            this.arrowPlayer1.Name = "arrowPlayer1";
            this.arrowPlayer1.Size = new System.Drawing.Size(36, 36);
            this.arrowPlayer1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.arrowPlayer1.TabIndex = 44;
            this.arrowPlayer1.TabStop = false;
            this.arrowPlayer1.Visible = false;
            // 
            // arrowPlayer2
            // 
            this.arrowPlayer2.BackColor = System.Drawing.Color.Transparent;
            this.arrowPlayer2.Image = global::WindowsFormsApplication4.Properties.Resources.turnArrow__2_;
            this.arrowPlayer2.Location = new System.Drawing.Point(940, 384);
            this.arrowPlayer2.Name = "arrowPlayer2";
            this.arrowPlayer2.Size = new System.Drawing.Size(36, 36);
            this.arrowPlayer2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.arrowPlayer2.TabIndex = 45;
            this.arrowPlayer2.TabStop = false;
            this.arrowPlayer2.Visible = false;
            // 
            // start
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::WindowsFormsApplication4.Properties.Resources.map2;
            this.ClientSize = new System.Drawing.Size(1004, 729);
            this.Controls.Add(this.arrowPlayer2);
            this.Controls.Add(this.arrowPlayer1);
            this.Controls.Add(this.exit);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.card10);
            this.Controls.Add(this.card9);
            this.Controls.Add(this.card7);
            this.Controls.Add(this.card5);
            this.Controls.Add(this.card4);
            this.Controls.Add(this.card3);
            this.Controls.Add(this.card2);
            this.Controls.Add(this.card1);
            this.Controls.Add(this.gold_card);
            this.Controls.Add(this.hp2);
            this.Controls.Add(this.gold_player2);
            this.Controls.Add(this.gold_player1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.hp1);
            this.Controls.Add(this.show_card);
            this.Controls.Add(this.hp_show2);
            this.Controls.Add(this.hp_show1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.name_show2);
            this.Controls.Add(this.name_show1);
            this.Controls.Add(this.name_turn);
            this.Controls.Add(this.hp_player2);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.hp_player1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.player2);
            this.Controls.Add(this.player1);
            this.Controls.Add(this.dices_clicked);
            this.Controls.Add(this.dices);
            this.Controls.Add(this.showRound);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.Name = "start";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dices)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dices_clicked)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.player1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.player2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.show_card)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hp1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hp2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gold_card)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.card1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.card2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.card3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.card4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.card5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.card7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.card9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.card10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arrowPlayer1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arrowPlayer2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label showRound;
        private System.Windows.Forms.PictureBox dices;
        private System.Windows.Forms.PictureBox dices_clicked;
        private System.Windows.Forms.PictureBox player1;
        private System.Windows.Forms.PictureBox player2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ProgressBar hp_player1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.ProgressBar hp_player2;
        private System.Windows.Forms.Label name_turn;
        private System.Windows.Forms.Label name_show1;
        private System.Windows.Forms.Label name_show2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label hp_show1;
        private System.Windows.Forms.Label hp_show2;
        private System.Windows.Forms.PictureBox show_card;
        private System.Windows.Forms.PictureBox hp1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label gold_player1;
        private System.Windows.Forms.Label gold_player2;
        private System.Windows.Forms.PictureBox hp2;
        private System.Windows.Forms.PictureBox gold_card;
        private System.Windows.Forms.PictureBox card1;
        private System.Windows.Forms.PictureBox card2;
        private System.Windows.Forms.PictureBox card3;
        private System.Windows.Forms.PictureBox card4;
        private System.Windows.Forms.PictureBox card5;
        private System.Windows.Forms.PictureBox card7;
        private System.Windows.Forms.PictureBox card9;
        private System.Windows.Forms.PictureBox card10;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label exit;
        private System.Windows.Forms.PictureBox arrowPlayer1;
        private System.Windows.Forms.PictureBox arrowPlayer2;
    }
}

