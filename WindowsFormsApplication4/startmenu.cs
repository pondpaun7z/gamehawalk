﻿using System;
using System.Media;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication4
{
    public partial class startmenu : Form
    {
        SoundPlayer sound = new SoundPlayer(Properties.Resources.strat_sound);
        public startmenu()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;
            sound.Play();
        }

        private void howtoplay_Click_1(object sender, EventArgs e)
        {
            howtoplay how_menu = new howtoplay();
            this.Hide();
            how_menu.ShowDialog();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            sound.Stop();
            Environment.Exit(1);
        }

        private void start_Click(object sender, EventArgs e)
        {
            create create_menu = new create();
            this.Hide();
            create_menu.ShowDialog();
            this.Close();
        }
    }
}
