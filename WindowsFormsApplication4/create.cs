﻿using System;
using System.Media;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication4
{
    public partial class create : Form
    {
        SoundPlayer sound = new SoundPlayer(Properties.Resources.create_sound);       

        public create()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;
            sound.Play();
        }

        private void start_Click(object sender, EventArgs e)
        {
            if (player1.Text == "" || player2.Text == "")
            {
                MyMessageBox.ShowBox("Please input name");
            }
            else
            {
                start inputName = new start(player1.Text, player2.Text);
                this.Hide();
                inputName.ShowDialog();
                this.Close();
            }
        }
    }
}
