﻿namespace WindowsFormsApplication4
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pl1 = new System.Windows.Forms.PictureBox();
            this.pl2 = new System.Windows.Forms.PictureBox();
            this.ch1att = new System.Windows.Forms.PictureBox();
            this.ch1def = new System.Windows.Forms.PictureBox();
            this.ch1set = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.Skill = new System.Windows.Forms.Label();
            this.ch1picskill = new System.Windows.Forms.PictureBox();
            this.none = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.ch2attskill = new System.Windows.Forms.PictureBox();
            this.ch2picskill = new System.Windows.Forms.PictureBox();
            this.ch2set = new System.Windows.Forms.PictureBox();
            this.ch2def = new System.Windows.Forms.PictureBox();
            this.ch2att = new System.Windows.Forms.PictureBox();
            this.att_show = new System.Windows.Forms.Label();
            this.skill1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.ch2ball = new System.Windows.Forms.PictureBox();
            this.ch2ballatt = new System.Windows.Forms.PictureBox();
            this.skill_b = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch1att)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch1def)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch1set)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch1picskill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.none)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch2attskill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch2picskill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch2set)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch2def)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch2att)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.skill1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch2ball)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch2ballatt)).BeginInit();
            this.SuspendLayout();
            // 
            // pl1
            // 
            this.pl1.BackColor = System.Drawing.Color.Transparent;
            this.pl1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pl1.Location = new System.Drawing.Point(152, 262);
            this.pl1.Name = "pl1";
            this.pl1.Size = new System.Drawing.Size(200, 200);
            this.pl1.TabIndex = 1;
            this.pl1.TabStop = false;
            // 
            // pl2
            // 
            this.pl2.BackColor = System.Drawing.Color.Transparent;
            this.pl2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pl2.Location = new System.Drawing.Point(736, 262);
            this.pl2.Name = "pl2";
            this.pl2.Size = new System.Drawing.Size(200, 200);
            this.pl2.TabIndex = 2;
            this.pl2.TabStop = false;
            this.pl2.Click += new System.EventHandler(this.pl2_Click);
            // 
            // ch1att
            // 
            this.ch1att.BackColor = System.Drawing.Color.Transparent;
            this.ch1att.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ch1att.Image = global::WindowsFormsApplication4.Properties.Resources.pl1att;
            this.ch1att.Location = new System.Drawing.Point(393, 172);
            this.ch1att.Name = "ch1att";
            this.ch1att.Size = new System.Drawing.Size(10, 10);
            this.ch1att.TabIndex = 3;
            this.ch1att.TabStop = false;
            // 
            // ch1def
            // 
            this.ch1def.BackColor = System.Drawing.Color.Transparent;
            this.ch1def.Image = global::WindowsFormsApplication4.Properties.Resources.newch1def;
            this.ch1def.Location = new System.Drawing.Point(409, 172);
            this.ch1def.Name = "ch1def";
            this.ch1def.Size = new System.Drawing.Size(10, 10);
            this.ch1def.TabIndex = 4;
            this.ch1def.TabStop = false;
            // 
            // ch1set
            // 
            this.ch1set.BackColor = System.Drawing.Color.Transparent;
            this.ch1set.Image = global::WindowsFormsApplication4.Properties.Resources.pl1set;
            this.ch1set.Location = new System.Drawing.Point(377, 172);
            this.ch1set.Name = "ch1set";
            this.ch1set.Size = new System.Drawing.Size(10, 10);
            this.ch1set.TabIndex = 5;
            this.ch1set.TabStop = false;
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Skill
            // 
            this.Skill.AutoSize = true;
            this.Skill.BackColor = System.Drawing.Color.Transparent;
            this.Skill.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Skill.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.Skill.Location = new System.Drawing.Point(147, 513);
            this.Skill.Name = "Skill";
            this.Skill.Size = new System.Drawing.Size(66, 25);
            this.Skill.TabIndex = 7;
            this.Skill.Text = "Skill :";
            // 
            // ch1picskill
            // 
            this.ch1picskill.BackColor = System.Drawing.Color.Transparent;
            this.ch1picskill.Image = global::WindowsFormsApplication4.Properties.Resources.Untitleกกกกกกd_1;
            this.ch1picskill.Location = new System.Drawing.Point(425, 172);
            this.ch1picskill.Name = "ch1picskill";
            this.ch1picskill.Size = new System.Drawing.Size(10, 10);
            this.ch1picskill.TabIndex = 8;
            this.ch1picskill.TabStop = false;
            this.ch1picskill.Visible = false;
            // 
            // none
            // 
            this.none.BackColor = System.Drawing.Color.Transparent;
            this.none.Location = new System.Drawing.Point(442, 172);
            this.none.Name = "none";
            this.none.Size = new System.Drawing.Size(10, 10);
            this.none.TabIndex = 14;
            this.none.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Location = new System.Drawing.Point(302, 262);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(442, 200);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 15;
            this.pictureBox1.TabStop = false;
            // 
            // ch2attskill
            // 
            this.ch2attskill.BackColor = System.Drawing.Color.Transparent;
            this.ch2attskill.Image = global::WindowsFormsApplication4.Properties.Resources.ยิงธรรมดาสวยๆ;
            this.ch2attskill.Location = new System.Drawing.Point(799, 172);
            this.ch2attskill.Name = "ch2attskill";
            this.ch2attskill.Size = new System.Drawing.Size(10, 10);
            this.ch2attskill.TabIndex = 20;
            this.ch2attskill.TabStop = false;
            // 
            // ch2picskill
            // 
            this.ch2picskill.BackColor = System.Drawing.Color.Transparent;
            this.ch2picskill.Image = global::WindowsFormsApplication4.Properties.Resources.skillch2;
            this.ch2picskill.Location = new System.Drawing.Point(782, 172);
            this.ch2picskill.Name = "ch2picskill";
            this.ch2picskill.Size = new System.Drawing.Size(10, 10);
            this.ch2picskill.TabIndex = 19;
            this.ch2picskill.TabStop = false;
            this.ch2picskill.Visible = false;
            // 
            // ch2set
            // 
            this.ch2set.BackColor = System.Drawing.Color.Transparent;
            this.ch2set.Image = global::WindowsFormsApplication4.Properties.Resources.ch2set;
            this.ch2set.Location = new System.Drawing.Point(734, 172);
            this.ch2set.Name = "ch2set";
            this.ch2set.Size = new System.Drawing.Size(10, 10);
            this.ch2set.TabIndex = 18;
            this.ch2set.TabStop = false;
            // 
            // ch2def
            // 
            this.ch2def.BackColor = System.Drawing.Color.Transparent;
            this.ch2def.Image = global::WindowsFormsApplication4.Properties.Resources.ch2def;
            this.ch2def.Location = new System.Drawing.Point(766, 172);
            this.ch2def.Name = "ch2def";
            this.ch2def.Size = new System.Drawing.Size(10, 10);
            this.ch2def.TabIndex = 17;
            this.ch2def.TabStop = false;
            // 
            // ch2att
            // 
            this.ch2att.BackColor = System.Drawing.Color.Transparent;
            this.ch2att.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ch2att.Image = global::WindowsFormsApplication4.Properties.Resources.ch2set1;
            this.ch2att.Location = new System.Drawing.Point(750, 172);
            this.ch2att.Name = "ch2att";
            this.ch2att.Size = new System.Drawing.Size(10, 10);
            this.ch2att.TabIndex = 16;
            this.ch2att.TabStop = false;
            // 
            // att_show
            // 
            this.att_show.AutoSize = true;
            this.att_show.BackColor = System.Drawing.Color.Transparent;
            this.att_show.Font = new System.Drawing.Font("Comic Sans MS", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.att_show.ForeColor = System.Drawing.Color.Red;
            this.att_show.Location = new System.Drawing.Point(741, 215);
            this.att_show.Name = "att_show";
            this.att_show.Size = new System.Drawing.Size(0, 38);
            this.att_show.TabIndex = 21;
            // 
            // skill1
            // 
            this.skill1.Cursor = System.Windows.Forms.Cursors.No;
            this.skill1.Image = global::WindowsFormsApplication4.Properties.Resources.Untitled_3;
            this.skill1.Location = new System.Drawing.Point(458, 172);
            this.skill1.Name = "skill1";
            this.skill1.Size = new System.Drawing.Size(13, 10);
            this.skill1.TabIndex = 22;
            this.skill1.TabStop = false;
            this.skill1.Visible = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Location = new System.Drawing.Point(349, 262);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(188, 200);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 23;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Visible = false;
            // 
            // ch2ball
            // 
            this.ch2ball.BackColor = System.Drawing.Color.Transparent;
            this.ch2ball.Image = global::WindowsFormsApplication4.Properties.Resources.fireball1;
            this.ch2ball.Location = new System.Drawing.Point(815, 172);
            this.ch2ball.Name = "ch2ball";
            this.ch2ball.Size = new System.Drawing.Size(10, 10);
            this.ch2ball.TabIndex = 24;
            this.ch2ball.TabStop = false;
            // 
            // ch2ballatt
            // 
            this.ch2ballatt.BackColor = System.Drawing.Color.Transparent;
            this.ch2ballatt.Image = global::WindowsFormsApplication4.Properties.Resources.boomboom;
            this.ch2ballatt.Location = new System.Drawing.Point(831, 172);
            this.ch2ballatt.Name = "ch2ballatt";
            this.ch2ballatt.Size = new System.Drawing.Size(10, 10);
            this.ch2ballatt.TabIndex = 25;
            this.ch2ballatt.TabStop = false;
            // 
            // skill_b
            // 
            this.skill_b.Location = new System.Drawing.Point(228, 492);
            this.skill_b.Name = "skill_b";
            this.skill_b.Size = new System.Drawing.Size(75, 73);
            this.skill_b.TabIndex = 26;
            this.skill_b.UseVisualStyleBackColor = true;
            this.skill_b.Click += new System.EventHandler(this.skill_b_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::WindowsFormsApplication4.Properties.Resources._11350205_473844982774669_2134283591_n;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1008, 730);
            this.Controls.Add(this.skill_b);
            this.Controls.Add(this.ch2ballatt);
            this.Controls.Add(this.ch2ball);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.skill1);
            this.Controls.Add(this.att_show);
            this.Controls.Add(this.ch2attskill);
            this.Controls.Add(this.ch2picskill);
            this.Controls.Add(this.ch2set);
            this.Controls.Add(this.ch2def);
            this.Controls.Add(this.ch2att);
            this.Controls.Add(this.none);
            this.Controls.Add(this.ch1picskill);
            this.Controls.Add(this.Skill);
            this.Controls.Add(this.ch1set);
            this.Controls.Add(this.ch1def);
            this.Controls.Add(this.ch1att);
            this.Controls.Add(this.pl1);
            this.Controls.Add(this.pl2);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form2";
            this.Text = "Form2";
            ((System.ComponentModel.ISupportInitialize)(this.pl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch1att)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch1def)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch1set)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch1picskill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.none)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch2attskill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch2picskill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch2set)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch2def)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch2att)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.skill1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch2ball)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch2ballatt)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pl1;
        private System.Windows.Forms.PictureBox pl2;
        private System.Windows.Forms.PictureBox ch1att;
        private System.Windows.Forms.PictureBox ch1def;
        private System.Windows.Forms.PictureBox ch1set;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label Skill;
        private System.Windows.Forms.PictureBox ch1picskill;
        private System.Windows.Forms.PictureBox none;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox ch2attskill;
        private System.Windows.Forms.PictureBox ch2picskill;
        private System.Windows.Forms.PictureBox ch2set;
        private System.Windows.Forms.PictureBox ch2def;
        private System.Windows.Forms.PictureBox ch2att;
        private System.Windows.Forms.Label att_show;
        private System.Windows.Forms.PictureBox skill1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox ch2ball;
        private System.Windows.Forms.PictureBox ch2ballatt;
        private System.Windows.Forms.Button skill_b;
    }
}

