﻿using System;
using System.Media;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication4
{
    public partial class win2 : Form
    {
        SoundPlayer sound = new SoundPlayer(Properties.Resources.win_sound);

        public win2()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;
            sound.Play();
        }

        private void back_Click(object sender, EventArgs e)
        {
            startmenu show = new startmenu();
            this.Hide();
            show.ShowDialog();
            this.Close();
        }

        private void exit_Click(object sender, EventArgs e)
        {
            Environment.Exit(1);            
        }
    }
}
