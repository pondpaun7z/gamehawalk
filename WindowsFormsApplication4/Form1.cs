﻿using System;
using System.Media;
using System.Drawing;
using System.Windows.Forms;

namespace WindowsFormsApplication4
{
   
    public partial class start : Form
    {

        SoundPlayer sound = new SoundPlayer(Properties.Resources.stratgame_sound);
        Random rand = new Random();
        private int round = 0;
        private byte press = 0;
        private int turn;

        Player[] player = new Player[2];
        ProgressBar[] hp = new ProgressBar[2];
        PictureBox[] arrow = new PictureBox[2];
        Label[] hp_show = new Label[2];

        Points[] up = new Points[19];
        Points[] down = new Points[18];
        Points[] right = new Points[31];
        Points[] left = new Points[13];
        Points[] item = new Points[23];
        Points[] sword = new Points[24];
        Points[] star = new Points[20];
        

        public start(string name1, string name2)
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;

            turn = rand.Next(0, 2);
            if (turn == 0)
            {
                name_turn.Text = name2;
                MyMessageBox.ShowBox("TURN: " + name2);
            }
            else if (turn == 1)
            {
                name_turn.Text = name1 ;
                MyMessageBox.ShowBox("TURN: " + name1);
            }

            player[0] = new Player(player2, name2, gold_player2);
            player[1] = new Player(player1, name1, gold_player1);

            hp[0] = hp_player1;
            hp[1] = hp_player2;
            hp_show[0] = hp_show1;
            hp_show[1] = hp_show2;

            name_show1.Text = "NAME: " + name1;
            name_show2.Text = "NAME: " + name2;

            arrow[0] = arrowPlayer2;
            arrow[1] = arrowPlayer1;
            arrow[turn].Visible = true;
            arrow[turn].BringToFront();

            dices.BringToFront();
            setPositionUp();
            setPositionDown();
            setPositionRight();
            setPositionLeft();
            setPositionItem();
            setPositionSword();
            setPotitionStar();

            player[0].showGold().Text = player[0].getGold().ToString();
            player[1].showGold().Text = player[1].getGold().ToString();

            hp_show1.Text = hp[0].Value.ToString();
            hp_show2.Text = hp[1].Value.ToString();

            sound.Play();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (!dices.Enabled)
            {
                while (press < round)
                { //press < round
                    //capture up arrow key
                    if (keyData == Keys.Up && this.player[turn].getPic().Location.Y > 64 && checkPoint(up, this.player[turn].getPic().Location.X, this.player[turn].getPic().Location.Y))
                    {
                        arrow[turn].Location = new Point(this.arrow[turn].Location.X, this.arrow[turn].Location.Y - 72);
                        arrow[turn].Visible = false;

                        player[turn].getPic().Location = new Point(this.player[turn].getPic().Location.X, this.player[turn].getPic().Location.Y - 72);
                        showRound.Text = "GET: " + (round - press - 1).ToString() + " points";
                        press++;
                        pressReset();

                        return true;
                    }

                    //capture down arrow key
                    if (keyData == Keys.Down && this.player[turn].getPic().Location.Y < 568 && checkPoint(down, this.player[turn].getPic().Location.X, this.player[turn].getPic().Location.Y))
                    {
                        arrow[turn].Location = new Point(this.arrow[turn].Location.X, this.arrow[turn].Location.Y + 72);
                        arrow[turn].Visible = false;

                        player[turn].getPic().Location = new Point(this.player[turn].getPic().Location.X, this.player[turn].getPic().Location.Y + 72);
                        showRound.Text = "GET: " + (round - press - 1).ToString() + " points";
                        press++;
                        pressReset();



                        return true;
                    }

                    //capture left arrow key
                    if (keyData == Keys.Left && this.player[turn].getPic().Location.X > 3 && checkPoint(left, this.player[turn].getPic().Location.X, this.player[turn].getPic().Location.Y))
                    {
                        arrow[turn].Location = new Point(this.arrow[turn].Location.X - 72, this.arrow[turn].Location.Y);
                        arrow[turn].Visible = false;

                        player[turn].getPic().Location = new Point(this.player[turn].getPic().Location.X - 72, this.player[turn].getPic().Location.Y);
                        showRound.Text = "GET: " + (round - press - 1).ToString() + " points";
                        press++;
                        pressReset();

                        return true;
                    }

                    //capture right arrow key
                    if (keyData == Keys.Right && this.player[turn].getPic().Location.X < 938 && checkPoint(right, this.player[turn].getPic().Location.X, this.player[turn].getPic().Location.Y))
                    {
                        arrow[turn].Location = new Point(this.arrow[turn].Location.X + 72, this.arrow[turn].Location.Y);
                        arrow[turn].Visible = false;

                        player[turn].getPic().Location = new Point(this.player[turn].getPic().Location.X + 72, this.player[turn].getPic().Location.Y);
                        showRound.Text = "GET: " + (round - press - 1).ToString() + " points";
                        press++;
                        pressReset();

                        //DEBUG: label6.Text = "X: " + this.player[turn].getPic().Location.X;
                        //DEBUG: showRound.Text = "Y: " + this.player[turn].getPic().Location.Y;

                        return true;
                    }

                    return base.ProcessCmdKey(ref msg, keyData);
                } 
            } else {
                if (keyData == Keys.Space)
                {
                    dices_clicked.BringToFront();
                    round = rand.Next(1, 7);
                    showRound.Text = "GET: " + round.ToString() + " points";
                    dices.Enabled = false;

                    return true;
                }
            }
            return false;
        }

        private void pressReset() {
            if (press == round)
            {
                checkEvent(); // item, star, sword
                dices.Enabled = true;
                dices.BringToFront();
                showRound.Text = "Click or Spacebar";
                press = 0;
                round = 0;
                if (turn == 0)
                {
                    turn = 1;
                    name_turn.Text = player[turn].getName() ;
                    MyMessageBox.ShowBox("TURN: " + player[turn].getName() );
                    
                }
                else if (turn == 1)
                {
                    turn = 0;
                    name_turn.Text = player[turn].getName() ;
                    MyMessageBox.ShowBox("TURN: " + player[turn].getName() );
                    
                }
                arrow[turn].Visible = true;
                arrow[turn].BringToFront();
            }
        }

        private bool checkPoint(Points[] pos, int x, int y)
        {
            for (int i = 0; i < pos.Length; i++)
            {
                if (pos[i].getX() == x && pos[i].getY() == y)
                {
                    return true;
                }
            }
            return false;
        }

        // Dice
        private void dices_Click(object sender, EventArgs e)
        {
            dices_clicked.BringToFront();
            round = rand.Next(1, 7);
            showRound.Text = "GET: " + round.ToString() + " points";
            dices.Enabled = false;
        }  

        private void checkEvent()
        {
            if (checkPoint(item, this.player[turn].getPic().Location.X, this.player[turn].getPic().Location.Y))
            {
                int gold = rand.Next(1, 50);
                dices.Visible = false;
                dices_clicked.Visible = false;
                show_card.Visible = true;
                show_card.Image = gold_card.BackgroundImage;
                show_card.BringToFront();
                MyMessageBox.ShowBox("คุณได้รับเงิน " + gold);
                player[turn].setGold(player[turn].getGold() + gold);
                player[turn].showGold().Text = player[turn].getGold().ToString();
                show_card.Visible = false;
                dices.Visible = true;
                dices_clicked.Visible = true;
            }
            else if (checkPoint(sword, this.player[turn].getPic().Location.X, this.player[turn].getPic().Location.Y))
            {
                MyMessageBox.ShowBox("คุณเป็นฝ่ายได้โจมตี");
                Form2 fight = new Form2(turn, player[turn].getStatus());
                fight.ShowDialog();
                if (hp[turn].Value - fight.getAtt() <= 0)
                {
                    hp[turn].Value = 0;
                    hp_show[turn].Text = hp[turn].Value.ToString();
                    MyMessageBox.ShowBox(player[turn].getName() + " Win!!");
                    checkWinner();
                } else
                {
                    hp[turn].Value -= fight.getAtt();
                    hp_show[turn].Text = hp[turn].Value.ToString();
                    player[turn].setStatus(fight.getStatus());
                }
            }
            else if (checkPoint(star, this.player[turn].getPic().Location.X, this.player[turn].getPic().Location.Y))
            {
                int card_rand = rand.Next(1, 9);
                checkCard(card_rand);
                MyMessageBox.ShowBox("คุณได้รับการเสี่ยงโชค");
                show_card.Visible = false;
            }
        }

        private void checkCard(int card) 
        {
            int gold_rand;
        
            if (card == 1) //พนัน
            {
                gold_rand = rand.Next(-20, 20);
                show_card.Image = card1.BackgroundImage;
                show_card.BringToFront();
                show_card.Visible = true;

                player[turn].setGold(player[turn].getGold() + gold_rand);
                player[turn].showGold().Text = player[turn].getGold().ToString();
            } else if (card == 2)   //ผ้าป่า
            {
                show_card.Image = card2.BackgroundImage;
                show_card.BringToFront();
                show_card.Visible = true;

                if (turn == 0) turn = 1; else if (turn == 1) turn = 0;
                player[turn].setGold(player[turn].getGold() + 30);
                player[turn].showGold().Text = player[turn].getGold().ToString();
                if (turn == 0) turn = 1; else if (turn == 1) turn = 0;

                int att = rand.Next(10, 20);
                if (hp[turn].Value - att <= 0)
                {
                    MyMessageBox.ShowBox(player[turn].getName() + " Win!!");
                    checkWinner();
                } else
                {
                    hp[turn].Value -= att;
                    hp_show[turn].Text = hp[turn].Value.ToString();
                }
                
            }
            else if (card == 3) // ต่อต่อย
            {
                show_card.Image = card3.BackgroundImage;
                show_card.BringToFront();
                show_card.Visible = true;
                if (turn == 0) turn = 1; else if (turn == 1) turn = 0;
                if (hp[turn].Value - 5 <= 0)
                {
                    MyMessageBox.ShowBox(player[turn].getName() + " Win!!");
                    checkWinner();
                }
                hp[turn].Value -= 5;
                hp_show[turn].Text = hp[turn].Value.ToString();
                if (turn == 0) turn = 1; else if (turn == 1) turn = 0;
            }
            else if (card == 4) // บริจาคเงิน
            {
                show_card.Image = card4.BackgroundImage;
                show_card.BringToFront();
                show_card.Visible = true;
                player[turn].setGold(player[turn].getGold() - 20);
                player[turn].showGold().Text = player[turn].getGold().ToString();
            }
            else if (card == 5) // บริจากเลือด
            {
                show_card.Image = card5.BackgroundImage;
                show_card.BringToFront();
                show_card.Visible = true;
                if (turn == 0) turn = 1; else if (turn == 1) turn = 0;
                if (hp[turn].Value - 20 <= 0)
                {
                    hp[turn].Value = 0;
                    hp_show[turn].Text = hp[turn].Value.ToString();
                    MyMessageBox.ShowBox(player[turn].getName() + " Win!!");
                    checkWinner();
                }
                else
                {
                    hp[turn].Value -= 20;
                    hp_show[turn].Text = hp[turn].Value.ToString();
                }
                if (turn == 0) turn = 1; else if (turn == 1) turn = 0;
            }
            else if (card == 6)
            {
                show_card.Image = card7.BackgroundImage;
                show_card.BringToFront();
                show_card.Visible = true;
                if (turn == 0) turn = 1; else if (turn == 1) turn = 0;
                if (hp[turn].Value - 20 <= 0)
                {
                    hp[turn].Value = 0;
                    hp_show[turn].Text = hp[turn].Value.ToString();
                    MyMessageBox.ShowBox(player[turn].getName() + " Win!!");
                    checkWinner();
                }
                else
                {
                    hp[turn].Value -= 20;
                    hp_show[turn].Text = hp[turn].Value.ToString();
                }
                if (turn == 0) turn = 1; else if (turn == 1) turn = 0;
            }
            else if (card == 7) // ยุงกัด
            {
                show_card.Image = card9.BackgroundImage;
                show_card.BringToFront();
                show_card.Visible = true;
                if (turn == 0) turn = 1; else if (turn == 1) turn = 0;
                if (hp[turn].Value + 20 >= 100)
                {
                    hp[turn].Value = 100;
                }
                else
                {
                    hp[turn].Value += 20;
                    hp_show[turn].Text = hp[turn].Value.ToString();
                }
                if (turn == 0) turn = 1; else if (turn == 1) turn = 0;
            }
            else if (card == 8)
            {
                show_card.Image = card10.BackgroundImage;
                show_card.BringToFront();
                show_card.Visible = true;

                player[turn].setGold(player[turn].getGold() - 10);
                player[turn].showGold().Text = player[turn].getGold().ToString();
            }
        }

        private void checkWinner()
        {
            win1 show1 = new win1();
            win2 show2 = new win2();
            if (turn == 0 )
            {
                this.Hide();       
                show1.ShowDialog();
                this.Close();
            } else if (turn == 1)
            {
                this.Hide();
                show2.ShowDialog();
                this.Close();
            }
        }

        // Set position sword
        private void setPositionSword()
        {
            sword[0] = new Points(722, 54);
            sword[1] = new Points(650, 54);
            sword[2] = new Points(74, 126);
            sword[3] = new Points(2, 342);
            sword[4] = new Points(146, 414);
            sword[5] = new Points(290, 342);
            sword[6] = new Points(506, 270);
            sword[7] = new Points(362, 54);
            sword[8] = new Points(578, 414);
            sword[9] = new Points(794, 198);
            sword[10] = new Points(938, 486);
            sword[11] = new Points(362, 486);
            sword[12] = new Points(938, 270);
            sword[13] = new Points(938, 126);
            sword[14] = new Points(506, 54);
            sword[15] = new Points(146, 126);
            sword[16] = new Points(146, 558);
            sword[17] = new Points(362, 558);
            sword[18] = new Points(290, 414);
            sword[19] = new Points(578, 198);
            sword[20] = new Points(578, 342);
            sword[21] = new Points(722, 558);
            sword[22] = new Points(866, 270);
            sword[23] = new Points(938, 270);
        }

        // Set star
        private void setPotitionStar()
        {
            star[0] = new Points(218, 54);
            star[1] = new Points(146, 54);
            star[2] = new Points(146, 270);            
            star[3] = new Points(722, 414);
            star[4] = new Points(938, 198);
            star[5] = new Points(578, 126);
            star[6] = new Points(218, 558);
            star[7] = new Points(794, 558);
            star[8] = new Points(722, 270);
            star[9] = new Points(866, 54);
            star[10] = new Points(794, 54);
            star[11] = new Points(2, 270);
            star[12] = new Points(74, 342);
            star[13] = new Points(74, 414);
            star[14] = new Points(362, 342);
            star[15] = new Points(362, 270);
            star[16] = new Points(506, 198);
            star[17] = new Points(650, 342);
            star[18] = new Points(650, 558);
            star[19] = new Points(794, 414);
        }

        // Set position item
        private void setPositionItem()
        {
            item[0] = new Points(2, 126);
            item[1] = new Points(74,486);
            item[2] = new Points(362, 414);
            item[3] = new Points(650, 486);
            item[4] = new Points(938, 54);
            item[5] = new Points(650, 198);
            item[6] = new Points(938, 342);
            item[7] = new Points(578, 54);
            item[8] = new Points(434, 54);
            item[9] = new Points(290, 54);
            item[10] = new Points(2, 198);
            item[11] = new Points(74, 558);
            item[12] = new Points(290, 558);
            item[13] = new Points(146, 342);
            item[14] = new Points(218, 414);
            item[15] = new Points(434, 270);
            item[16] = new Points(506, 342);
            item[17] = new Points(650, 414);
            item[18] = new Points(866, 558);
            item[19] = new Points(938, 558);
            item[20] = new Points(650, 270);
            item[21] = new Points(794, 270);
            item[22] = new Points(794, 126);
        }

        // Set position for press(up, down, right, left)
        private void setPositionUp()
        {
            up[0] = new Points(362, 558);
            up[1] = new Points(362, 486);
            up[2] = new Points(362, 414);
            up[3] = new Points(290, 414);
            up[4] = new Points(362, 342);
            up[5] = new Points(506, 270);
            up[6] = new Points(938, 558);
            up[7] = new Points(938, 486);
            up[8] = new Points(938, 414);
            up[9] = new Points(938, 342);
            up[10] = new Points(938, 270);
            up[11] = new Points(938, 198);
            up[12] = new Points(938, 126);
            up[13] = new Points(794, 270);
            up[14] = new Points(794, 198);
            up[15] = new Points(794, 126);
            up[16] = new Points(794, 270);
            up[17] = new Points(578, 126);
            up[18] = new Points(578, 198);
        }

        private void setPositionDown()
        {
            down[0] = new Points(146, 54);
            down[1] = new Points(146, 126);
            down[2] = new Points(2, 126);
            down[3] = new Points(2, 198);
            down[4] = new Points(2, 270);
            down[5] = new Points(146, 198);
            down[6] = new Points(146, 342);
            down[7] = new Points(74, 342);
            down[8] = new Points(74, 414);
            down[9] = new Points(506, 270);
            down[10] = new Points(650, 198);
            down[11] = new Points(650, 270);
            down[12] = new Points(650, 342);
            down[13] = new Points(650, 414);
            down[14] = new Points(650, 486);
            down[15] = new Points(578, 342);
            down[16] = new Points(146, 270);
            down[17] = new Points(74, 486);
        }

        private void setPositionRight()
        {
            right[0] = new Points(2, 342);
            right[1] = new Points(74, 342);
            right[2] = new Points(74, 414);
            right[3] = new Points(146, 414);
            right[4] = new Points(218, 414);
            right[5] = new Points(290, 414);
            right[6] = new Points(290, 342);
            right[7] = new Points(74, 558);
            right[8] = new Points(146, 558);
            right[9] = new Points(218, 558);
            right[10] = new Points(290, 558);
            right[11] = new Points(362, 270);
            right[12] = new Points(434, 270);
            right[13] = new Points(506, 198);
            right[14] = new Points(578, 198);
            right[15] = new Points(506, 342);
            right[16] = new Points(578, 342);
            right[17] = new Points(578, 414);
            right[18] = new Points(650, 414);
            right[19] = new Points(722, 414);
            right[20] = new Points(794, 414);
            right[21] = new Points(866, 414);
            right[22] = new Points(650, 558);
            right[23] = new Points(722, 558);
            right[24] = new Points(794, 558);
            right[25] = new Points(866, 558);
            right[26] = new Points(650, 558);
            right[27] = new Points(650, 270);
            right[28] = new Points(722, 270);
            right[29] = new Points(794, 270);
            right[30] = new Points(866, 270);
        }

        private void setPositionLeft()
        {
            left[0] = new Points(938, 54);
            left[1] = new Points(866, 54);
            left[2] = new Points(794, 54);
            left[3] = new Points(722, 54);
            left[4] = new Points(650, 54);
            left[5] = new Points(578, 54);
            left[6] = new Points(506, 54);
            left[7] = new Points(434, 54);
            left[8] = new Points(362, 54);
            left[9] = new Points(290, 54);
            left[10] = new Points(218, 54);
            left[11] = new Points(146, 126);
            left[12] = new Points(74, 126);
        }

        private void hp1_Click(object sender, EventArgs e)
        {

            if (player[turn].getGold() < 0 && turn == 1)
            {
                MyMessageBox.ShowBox("คุณไม่มีเงิน แถมยังเป็นหนี้อีก");
                return;
            }
            else if (hp_player1.Value >= 80 && turn == 1)
            {
                MyMessageBox.ShowBox("ต้องมีเลือกน้อยกว่า 80");
            }
            else if (turn == 1 && player[turn].getGold() >= 10)
            { 
                if (hp_player1.Value >= 100)
                {
                    hp_player1.Value = 100;
                    MyMessageBox.ShowBox("เลือดของคุณเต็มแล้ว");
                    return;
                } else
                {
                    hp_player1.Value += 5;
                    player[turn].setGold(player[turn].getGold() - 10);
                    hp_show1.Text = hp_player1.Value.ToString();
                    player[turn].showGold().Text = player[turn].getGold().ToString();
                }
                
            } else if (turn == 1){
                MyMessageBox.ShowBox("เงินไม่เพียงพอ");
            }
        }

        private void hp2_Click(object sender, EventArgs e)
        {
            if (player[turn].getGold() < 0 && turn == 0)
            {
                MyMessageBox.ShowBox("คุณไม่มีเงิน แถมยังเป็นหนี้อีก");
                return;
            } else if (hp_player2.Value >= 80 && turn == 0)
            {
                MyMessageBox.ShowBox("ต้องมีเลือกน้อยกว่า 80");
                return;
            } else if (turn == 0 && player[turn].getGold() >= 10)
            {
                if (hp_player2.Value >= 100)
                {
                    hp_player2.Value = 100;
                    MyMessageBox.ShowBox("เลือดของคุณเต็มแล้ว");
                    return;
                } else
                {
                    hp_player2.Value += 5;
                    player[turn].setGold(player[turn].getGold() - 10);
                    hp_show2.Text = hp_player2.Value.ToString();
                    player[turn].showGold().Text = player[turn].getGold().ToString();
                }
            }
            else if (turn == 0)
            {
                MyMessageBox.ShowBox("เงินไม่เพียงพอ");
            }
        }

        private void exit_Click(object sender, EventArgs e)
        {
            Environment.Exit(1);
        }
    }

    public class Points
    {
        private int x;
        private int y;

        public Points (int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public int getX()
        {
            return x;
        }

        public int getY()
        {
            return y;
        }        
    }

    public class Player
    {
        private PictureBox player;
        private byte status_skill;
        private string name;
        private int gold;
        private Label goldshow;
        

        public Player(PictureBox player, string name, Label showG)
        {
            this.player = player;
            status_skill = 0;
            this.name = name;
            gold = 0;
            goldshow = showG;
        }

        public PictureBox getPic()
        {
            return player;
        }

        public void setStatus(byte stat)
        {
            status_skill = stat;
        }

        public byte getStatus()
        {
            return status_skill;
        }
       
        public string getName()
        {
            return name;
        }

        public void setGold(int gold)
        {
            this.gold = gold;
        }

        public int getGold()
        {
            return gold;
        }

        public Label showGold()
        {
            return goldshow;
        }
    }
}


